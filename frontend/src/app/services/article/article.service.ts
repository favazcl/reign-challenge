import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article } from '../../models/article';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  private articleURL: string = environment.articleURL;

  constructor(private httpClient: HttpClient) { }

  public getArticles(): Observable<Article[]> {
    return new Observable<Article[]>(observe => {
      this.httpClient.get(this.articleURL).subscribe((data: any) => {
        const article = data.map((tmpArticle: Article) => {
          return new Article(tmpArticle);
        });
        observe.next(article);
        observe.complete();
      }, error => {
        console.log('Error on getArticles: ' + error);
        observe.next([]);
        observe.complete();
      });
    });
  }

  public getExternalArticles(): Observable<boolean> {
    return new Observable<boolean>(observe => {
      this.httpClient.get<boolean>(`${this.articleURL}${'external'}`).subscribe((data: any) => {
        observe.next(true);
        observe.complete();
      }, error => {
        console.log('Error on getExternalArticles: ' + error);
        observe.next(false);
        observe.complete();
      });
    });
  }

  public getArticle(articleId: number): Observable<Article> {
    return this.httpClient.get<Article>(`${this.articleURL}${articleId}`);
  }

  public createArticle(article: Article): Observable<Article> {
    return this.httpClient.post<Article>(this.articleURL, article);
  }

  public updateArticle(article: Article): Observable<boolean> {
    return new Observable<boolean>(observe => {
      this.httpClient.patch<Article>(`${this.articleURL}${article.id}`, article.toJSON()).subscribe((data: any) => {
        observe.next(true);
        observe.complete();
      }, error => {
        console.log('Error on updateArticle: ' + error);
        observe.complete();
      });
    });
  }

  public deleteArticle(articleId: string): Observable<boolean> {
    return new Observable<boolean>(observe => {
      this.httpClient.delete(`${this.articleURL}${articleId}`).subscribe((data: any) => {
        observe.next(true);
        observe.complete();
      }, error => {
        console.log('Error on getArticles: ' + error);
        observe.next(false);
        observe.complete();
      });
    });
  }
}
