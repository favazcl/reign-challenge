import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article/article.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  public articles: Article[] = [];

  constructor(private articleService: ArticleService) {
    this.getArticles();
  }

  ngOnInit(): void {
    this.getExternalArticles();
  }

  public getArticles(): void {
    this.articleService.getArticles().subscribe((data: Article[]) => {
      this.articles = [...data];
    }, err => {
      console.log('Error on getArticles' + err);
    });
  }

  public getExternalArticles(): void {
    this.articleService.getExternalArticles().subscribe((data: boolean) => {
      this.getArticles();
    });
  }

  public blockArticle(article: Article): void {
    article.blockedUp = true;
    this.articleService.updateArticle(article).subscribe((data: boolean) => {
      if (data) {
        this.getArticles();
      }
    }, err => {
      console.log('Error on deleteArticle' + err);
    });
  }

}
