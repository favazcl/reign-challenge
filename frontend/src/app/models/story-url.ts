export class StoryUrl {
  public value: string;
  public matchLevel: string;
  public matchedWords: any[];

  constructor(value: string,
              matchLevel: string,
              matchedWords: any[]) {
    this.value = value;
    this.matchLevel = matchLevel;
    this.matchedWords = matchedWords;
  }
}
