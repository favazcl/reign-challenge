import { HighlightResult } from './highlight-result';

export class Article {
  public id?: string;
  public createdAt?: string;
  public title?: string;
  public url?: string;
  public author?: string;
  public points?: number;
  public storyText?: string;
  public commentText?: string;
  public numComments?: number;
  public storyId?: number;
  public storyTitle?: string;
  public storyUrl?: string;
  public parentId?: number;
  public createdAtI?: number;
  public tags?: string[];
  public objectId?: string;
  public highlightResult?: HighlightResult;
  public blockedUp?: boolean;

  constructor(data: any = null) {
    this.update(data);
  }

  public update(data: any = null) {
    if (data) {
      this.id = data._id;
      this.createdAt = data.created_at;
      this.title = data.title;
      this.url = data.url;
      this.author = data.author;
      this.points = data.points;
      this.storyText = data.story_text;
      this.commentText = data.comment_text;
      this.numComments = data.num_comments;
      this.storyId = data.story_id;
      this.storyTitle = data.story_title;
      this.storyUrl = data.story_url;
      this.parentId = data.parent_id;
      this.createdAtI = data.created_at_i;
      this.tags = data._tags;
      this.objectId = data.objectID;
      this.highlightResult = data._highlightResult;
      this.blockedUp = data.blockUp;
    }
  }

  public toJSON(): any {
    return {
      id: (this.id) ? this.id : undefined,
      created_at: (this.createdAt) ? this.createdAt : undefined,
      title: (this.title) ? this.title : undefined,
      url: (this.url) ? this.url : undefined,
      author: (this.author) ? this.author : undefined,
      points: (this.points) ? this.points : undefined,
      story_text: (this.storyText) ? this.storyText : undefined,
      comment_text: (this.commentText) ? this.commentText : undefined,
      story_id: (this.storyId) ? this.storyId : undefined,
      story_title: (this.storyTitle) ? this.storyTitle : undefined,
      story_url: (this.storyUrl) ? this.storyUrl : undefined,
      parent_id: (this.parentId) ? this.parentId : undefined,
      created_at_i: (this.createdAtI) ? this.createdAtI : undefined,
      _tags: (this.tags) ? this.tags : undefined,
      objectID: (this.objectId) ? this.objectId : undefined,
      _highlightResult: (this.highlightResult) ? this.highlightResult : undefined,
      blockedUp: (this.blockedUp) ? this.blockedUp : undefined
    };
  }

}
