export class CommentText {
  public value: string;
  public matchLevel: string;
  public fullyHighlighted: boolean;
  public matchedWords: string[];

  constructor(value: string,
              matchLevel: string,
              fullyHighlighted: boolean,
              matchedWords: string[]) {
    this.value = value;
    this.matchLevel = matchLevel;
    this.fullyHighlighted = fullyHighlighted;
    this.matchedWords = matchedWords;
  }
}
