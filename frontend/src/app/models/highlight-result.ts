import { Author } from './author';
import { CommentText } from './comment-text';
import { StoryTitle } from './story-title';
import { StoryUrl } from './story-url';

export class HighlightResult {
  public author: Author;
  public commentText: CommentText;
  public storyTitle: StoryTitle;
  public storyUrl: StoryUrl;

  constructor(author: Author,
              commentText: CommentText,
              storyTitle: StoryTitle,
              storyUrl: StoryUrl) {
    this.author = author;
    this.commentText = commentText;
    this.storyTitle = storyTitle;
    this.storyUrl = storyUrl;
  }
}
