import {
  Controller,
  Get,
  Post,
  Delete,
  Body,
  Param,
  Patch,
} from '@nestjs/common';
import { IArticle } from './interfaces/article.interface';
import { ArticleService } from './article.service';
import { CreateArticleDTO } from './dto/article.dto';

@Controller('api/articles')
export class ArticleController {
  public constructor(private readonly articleService: ArticleService) {}

  @Post()
  public async createArticle(
    @Body() createArticleDTO: CreateArticleDTO,
  ): Promise<IArticle> {
    return await this.articleService.createArticle(createArticleDTO);
  }

  @Get('/external')
  public async getExternalArticles(): Promise<IArticle[]> {
    return await this.articleService.getExternalArticles();
  }

  @Get()
  public async getArticles(): Promise<IArticle[]> {
    return await this.articleService.getArticles();
  }

  @Get(':id')
  public async getArticle(@Param('id') articleId): Promise<IArticle> {
    return await this.articleService.getArticle(articleId);
  }

  @Patch(':id')
  public async updateArticle(
    @Body() createArticleDTO: CreateArticleDTO,
    @Param('id') articleId,
  ): Promise<IArticle> {
    return await this.articleService.updateArticle(articleId, createArticleDTO);
  }

  @Delete(':id')
  public async deleteArticle(@Param('id') articleId): Promise<IArticle> {
    return await this.articleService.deleteArticle(articleId);
  }
}
