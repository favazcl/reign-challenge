import { Test, TestingModule } from '@nestjs/testing';
import { ArticleController } from './article.controller';
import { ArticleService } from './article.service';
import { ARTICLE_OBJECT } from '../utils/articleObject';

describe('ArticleController', () => {
  let controller: ArticleController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ArticleController],
      providers: [
        {
          provide: ArticleService,
          useValue: {
            getArticles: jest.fn().mockResolvedValue([ARTICLE_OBJECT]),
            getArticle: jest
              .fn()
              .mockImplementation((articleId: number) =>
                Promise.resolve((ARTICLE_OBJECT.story_id = articleId)),
              ),
          },
        },
      ],
    }).compile();

    controller = module.get<ArticleController>(ArticleController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getArticles', () => {
    it('should return an array of articles', () => {
      expect(controller.getArticles()).resolves.toEqual([ARTICLE_OBJECT]);
    });
  });
});
