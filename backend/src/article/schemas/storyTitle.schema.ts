import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class StoryTitle extends Document {
  @Prop()
  value: string;

  @Prop()
  matchLevel: string;

  @Prop()
  matchedWords: any[];
}

export const CustomerSchema = SchemaFactory.createForClass(StoryTitle);
