import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class CommentText extends Document {
  @Prop()
  value: string;

  @Prop()
  matchLevel: string;

  @Prop()
  fullyHighlighted: boolean;

  @Prop()
  matchedWords: any[];
}

export const CustomerSchema = SchemaFactory.createForClass(CommentText);
