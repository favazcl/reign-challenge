import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Author extends Document {
  @Prop()
  value: string;

  @Prop()
  matchLevel: string;

  @Prop()
  fullyHighlighted: boolean;

  @Prop([String])
  matchedWords: string[];
}

export const CustomerSchema = SchemaFactory.createForClass(Author);
