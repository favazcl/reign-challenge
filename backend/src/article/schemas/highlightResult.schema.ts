import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Author } from './author.schema';
import { CommentText } from './commentText.schema';
import { StoryTitle } from './storyTitle.schema';
import { StoryUrl } from './storyUrl.schema';

@Schema()
export class HighlightResult extends Document {
  @Prop()
  author: Author;

  @Prop()
  comment_text: CommentText;

  @Prop()
  story_title: StoryTitle;

  @Prop()
  story_url: StoryUrl;
}

export const CustomerSchema = SchemaFactory.createForClass(HighlightResult);
