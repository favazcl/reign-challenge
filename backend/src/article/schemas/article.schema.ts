import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { HighlightResult } from './highlightResult.schema';

@Schema()
export class Article extends Document {
  @Prop()
  created_at: string;

  @Prop()
  title: string;

  @Prop()
  url: string;

  @Prop()
  author: string;

  @Prop()
  points: number;

  @Prop()
  story_text: string;

  @Prop()
  comment_text: string;

  @Prop()
  num_comments: number;

  @Prop({ unique: true })
  story_id: number;

  @Prop()
  story_title: string;

  @Prop()
  story_url: string;

  @Prop()
  parent_id: number;

  @Prop()
  created_at_i: number;

  @Prop([String])
  _tags: string[];

  @Prop()
  objectID: string;

  @Prop()
  _highlightResult: HighlightResult;

  @Prop()
  blockedUp: boolean;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
