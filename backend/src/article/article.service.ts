import { Article } from './schemas/article.schema';
import { CreateArticleDTO } from './dto/article.dto';
import { InjectModel } from '@nestjs/mongoose';
import { HttpException, HttpService, HttpStatus, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';

@Injectable()
export class ArticleService {
  public constructor(
    @InjectModel(Article.name) private readonly articleModel: Model<Article>,
    private readonly httpService: HttpService,
  ) { }

  public async createArticle(
    createArticleDTO: CreateArticleDTO,
  ): Promise<Article> {
    const exists: Article = await this.getArticle(createArticleDTO.story_id);

    if (!exists) {
      try {
        createArticleDTO.blockedUp = false;
        const article: Article = await new this.articleModel(
          createArticleDTO,
        ).save();

        if (!article) throw new HttpException('Cannot create article', 404);

        return article;
      } catch (error) {
        console.log('Error on createArticle: ' + error);
      }
    }
  }

  public async getExternalArticles(): Promise<Article[]> {
    const data: any = (
      await this.httpService
        .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
        .toPromise()
    ).data.hits;

    if (!data) throw new HttpException('Cannot get external articles', 404);

    const articles: Article[] = [];
    await Promise.all(
      data.map(async (e: CreateArticleDTO) => {
        e.blockedUp = false;
        const article: Article = await this.createArticle(e);

        if (article != undefined) {
          articles.push(article);
        }
      }),
    );

    if (!articles || articles.length == 0)
      throw new HttpException('Cannot get new articles from external', 404);

    return articles;

  }

  public async getArticles(): Promise<Article[]> {
    const articles: Article[] = await this.articleModel.find({ blockedUp: false }).sort({ "created_at": -1 });

    if (!articles) throw new HttpException('Cannot find articles', 404);

    return articles;
  }

  public async getArticle(articleId: number): Promise<Article> {
    const article: Article = await this.articleModel.findOne({
      story_id: articleId,
    });

    if (!article) return null;

    return article;
  }

  public async updateArticle(
    articleId: string,
    createArticleDTO: CreateArticleDTO,
  ): Promise<Article> {
    const tmpArticle: Article = await this.getArticle(
      createArticleDTO.story_id,
    );

    if (!tmpArticle)
      throw new HttpException('Cannot find article to updated', 404);

    const article = await this.articleModel.updateOne(
      { _id: articleId },
      createArticleDTO
    );

    if (!article) throw new HttpException('Cannot update article', 404);

    return article;
  }

  public async deleteArticle(articleId: string): Promise<Article> {
    const article: Article = await this.articleModel.findByIdAndDelete(
      articleId,
    );

    if (!article) throw new HttpException('Cannot find article to delete', 404);

    return article;
  }
}
