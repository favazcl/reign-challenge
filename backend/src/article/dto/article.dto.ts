import { HighlightResult } from '../schemas/highlightResult.schema';

export class CreateArticleDTO {
  public readonly created_at: string;
  public readonly title: string;
  public readonly url: string;
  public readonly author: string;
  public readonly points: number;
  public readonly story_text: string;
  public readonly comment_text: string;
  public readonly num_comments: number;
  public readonly story_id: number;
  public readonly story_title: string;
  public readonly story_url: string;
  public readonly parent_id: number;
  public readonly created_at_i: number;
  public readonly _tags: Array<string>;
  public readonly objectID: string;
  public readonly _highlightResult: HighlightResult;
  public blockedUp: boolean;
}
