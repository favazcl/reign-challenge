import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { ArticleService } from 'src/article/article.service';

@Injectable()
export class TasksService {

  constructor(private articleService: ArticleService) { }

  @Cron(CronExpression.EVERY_HOUR)
  async handleCron() {
    await this.articleService.getExternalArticles();
  }
}



