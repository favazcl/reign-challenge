export const ARTICLE_OBJECT = {
  _tags: ['comment', 'author_nobody9999', 'story_25485966'],
  _id: '5fdf93887ab94703584efc50',
  created_at: '2020-12-20T17:37:52.000Z',
  title: null,
  url: null,
  author: 'nobody9999',
  points: null,
  story_text: null,
  comment_text:
    '&gt;Municipal broadband is an interesting idea but physical infrastructure inevitably needs maintenance and I&#x27;m not sure tax dollars should be used to maintain fiber optic lines to residences.<p>There&#x27;s no need to lease the fiber infrastructure, just provide interconnect nodes for ISPs.<p>Municipal broadband maintenance can be paid for by charging interconnect fees to the ISPs who wish to service that area.<p>Once the infrastructure is in place, barriers to entry for ISPs is very low.  All you need is bandwidth and connection to the municipal network.<p>Maintenance can be provided on a cost plus basis by an independent (i.e., not the ISPs) company that manages and maintains the infrastructure.<p>And surpluses from interconnect fees can be used for both network enhancements and other municipal programs.',
  num_comments: null,
  story_id: 25485966,
  story_title:
    'Starting Sunday, cable companies can no longer ‘rent’ you the router you own',
  story_url:
    'https://www.theverge.com/2020/12/19/22191096/internet-modem-router-rental-fee-fix-television-viewer-protection-act-tvpa',
  parent_id: 25487289,
  created_at_i: 1608485872,
  objectID: '25487971',
  _highlightResult: {
    author: {
      value: 'nobody9999',
      matchLevel: 'none',
      matchedWords: [],
    },
    comment_text: {
      value:
        "&gt;Municipal broadband is an interesting idea but physical infrastructure inevitably needs maintenance and I'm not sure tax dollars should be used to maintain fiber optic lines to residences.<p>There's no need to lease the fiber infrastructure, just provide interconnect <em>nodes</em> for ISPs.<p>Municipal broadband maintenance can be paid for by charging interconnect fees to the ISPs who wish to service that area.<p>Once the infrastructure is in place, barriers to entry for ISPs is very low.  All you need is bandwidth and connection to the municipal network.<p>Maintenance can be provided on a cost plus basis by an independent (i.e., not the ISPs) company that manages and maintains the infrastructure.<p>And surpluses from interconnect fees can be used for both network enhancements and other municipal programs.",
      matchLevel: 'full',
      fullyHighlighted: false,
      matchedWords: ['nodejs'],
    },
    story_title: {
      value:
        'Starting Sunday, cable companies can no longer ‘rent’ you the router you own',
      matchLevel: 'none',
      matchedWords: [],
    },
    story_url: {
      value:
        'https://www.theverge.com/2020/12/19/22191096/internet-modem-router-rental-fee-fix-television-viewer-protection-act-tvpa',
      matchLevel: 'none',
      matchedWords: [],
    },
    blockedUp: false
  },
};
