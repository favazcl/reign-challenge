# REIGN CHALLENGE

## Introducción
Reign Challenge es el nombre de la aplicación web capaz de mostrar las últimas noticias obtenidas desde una API, ordenarlas desde 
la más reciente a la más antigua, además actualiza cada 1 hora las noticias y si eliminas una no se volverá a mostrar.

## Imagen demo

![front](/uploads/e4a3d77c7dba802befae5c3c1749db15/front.png)

## Dependencias

**Docker y Docker Compose**: Docker es la única dependencia utilizada por el sistema para la implementación en su sistema de contenedores.

### Ubuntu

```
## Agregue la clave GPG oficial de Docker:
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

## Utilice el siguiente comando para configurar el repositorio estable:
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
   
# Instalación de Docker Engine

## Actualice el índice del paquete apt e instale la última versión de Docker Engine y container, o vaya al siguiente paso para instalar una versión específica:
 $ sudo apt-get update
 $ sudo apt-get install docker-ce docker-ce-cli containerd.io
 
## Verifique que Docker Engine esté correctamente ejecutando la imagen hello-world:
$ sudo docker run hello-world

# Instalación de Docker Compose

## Ejecute este comando para descargar la versión estable actual de Docker Compose:
 $ sudo curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

## Aplicar permisos ejecutables al binario:
 $ sudo chmod +x /usr/local/bin/docker-compose
 
## Cree un enlace simbólico a /usr/bin o cualquier otro directorio en su ruta.
 $ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

```

### Otros sistemas basados en Linux

* For others linux-based systems visit this [LINK](https://docs.docker.com/engine/install/)

### Para OSX
* For OSX visit this [LINK](https://docs.docker.com/docker-for-mac/)

### Clonar el repositorio vía SSH

Para obtener el repositorio debes clonar el proyecto en tu máquina local, para hacer esto ingresa en tu terminal la siguiente declaración:
```
git clone git@gitlab.com:favazcl/reign-challenge.git
```

#### Dependencias de desarrollo

1. **NestJs**: El framework de NestJs se utiliza para el entorno de backend. Para instalarlo debes usar el siguiente comando:

```zsh
## With npm
sudo npm i -g @nestjs/cli


## Instalar dependencias de backend
npm install
```
2. **Angular CLI**: La CLI de Angular se utiliza para administrar el desarrollo, la construcción y la implementación de la aplicación frontend. Para instalarlo debes usar el siguiente comando:

```zsh
## With npm
sudo npm install -g @angular/cli

## Install frontend dependencies
npm install
```

#### Arquitectura de Contenedores

La aplicación esta dockerizada tanto para el cliente y el servidor, estos contenedores estan orquestados por un docker-compose que permite la comunicación entre los contenedores, además del levantamiento de la base de datos.

1. Contenedor servidor: En este contenedor esta el framework NestJs y se puede consumir la API a traves del puerto `8080`.
2. Contenedor cliente: En este contenedor esta el framework Angular y se puede acceder a traves del puerto `4200`
3. Contenedor de base de datos: En este contenedor esta el gestor de base de datos mongo y se puede acceder a traves del puerto `27017`

#### Levantamiento de la Aplicación

Para levantar la aplicación usted debe clonar o descargar el repositorio, luego de esto debe seguir los siguientes pasos:

1. Ingresar al directorio donde este ubicado el archivo `docker-compose.yml`
2. Ejecutar el siguiente comando: `docker-compose build`
3. Terminado este proceso, ejecutar el siguiente comando: `docker-compose up`

Como se menciono anteriormente una vez levantada la aplicación estaran disponibles los ambientes de cliente y servidor, accediendo a estos de la siguiente manera:

> Cliente: http://localhost:4200/

> Servidor: http://localhost:8080/

#### Integración Continua (CI)

El proyecto se realizo con la herramienta CI de gitlab, y se siguio un patron padre-hijo para los pipelines, quedando de la siguiente manera.

![pipeline](/uploads/e5a31fd06fa63810e7cda460d474dffb/pipeline.png)

Donde el padre ejecuta un activador tanto para el pipeline de frontend y backend, donde cada uno de ellos cuenta con las siguientes funcionalidades:

1. Los pipelines de frontend y backend ejecutan el comando linter para analizar el código.
2. Los pipelines de frontend y backend ejecutan el comando test para analizar el coverage de este.

### Tests y Linters

Para ejecutar los tests disponibles y linters debe realizar las siguientes instrucciones:

## Backend

1. Ingresar al directorio llamado backend.
2. Para los test ejecutar el siguiente comando: `npm run test:cov`
3. Para el linter ejecutar el siguiente comano: `npm run lint`

## Frontend

1. Ingresar al directorio llamado frontend.
2. Para los test ejecutar el siguiente comando: `npm run test`
3. Para el linter ejecutar el siguiente comando: `npm run lint`

